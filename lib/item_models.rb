# frozen_string_literal: true

require 'item_models/version'

module ItemModels
  class Error < StandardError; end
  # Your code goes here...
  def self.load
    require directory + '/lib/models/item_application_record'
    files.each {|file| require file }
  end

  private

  def self.files
    Dir[ directory + '/lib/models/*.rb']
  end

  def self.directory
    Gem::Specification.find_by_name("item_models_2").gem_dir
  end
end
