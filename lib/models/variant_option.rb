# frozen_string_literal: true

require 'concerns/restorable'
class VariantOption < ItemApplicationRecord
  self.table_name = 'item_variant_options'
  include ::Restorable

  has_many :variant_option_associations, foreign_key: :option_id
end
