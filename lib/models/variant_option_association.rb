# frozen_string_literal: true

require 'concerns/restorable'
class VariantOptionAssociation < ItemApplicationRecord
  self.table_name = 'item_variant_option_associations'
  include ::Restorable

  belongs_to :variant_option, foreign_key: :option_id
  belongs_to :variant, foreign_key: :variant_id
  belongs_to :option_type, foreign_key: :option_type_id
end
