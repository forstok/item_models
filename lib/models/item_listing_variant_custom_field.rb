# frozen_string_literal: true

class ItemListingVariantCustomField < ItemApplicationRecord
  self.table_name = 'item_channel_association_variant_custom_fields'

  belongs_to :item_listing, foreign_key: :channel_association_id
  belongs_to :variant
  belongs_to :image
  belongs_to :shopee,
    class_name: 'ShopeeItemVariantCustomField',
    foreign_key: :shopee_item_variant_custom_field_id,
    optional: true
  belongs_to :tokopedia,
    class_name: 'TokopediaItemVariantCustomField',
    foreign_key: :tokopedia_custom_field_id,
    optional: true
end
