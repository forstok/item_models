# frozen_string_literal: true

require 'concerns/restorable'
class ItemListing < ItemApplicationRecord
  self.table_name = 'item_channel_associations'
  include ::Restorable

  belongs_to :item
  has_many :variant_listings, foreign_key: :channel_association_id
  has_many :item_listing_images, foreign_key: :channel_association_id
  has_one :item_listing_category, foreign_key: :channel_association_id
  has_one :item_listing_brand, foreign_key: :channel_association_id
  has_many :item_listing_variant_custom_fields, foreign_key: :channel_association_id
  has_many :item_listing_custom_fields, foreign_key: :channel_association_id
  has_many :item_listing_variant_images, foreign_key: :channel_association_id
  has_many :images,
    class_name: 'ItemListingImage',
    dependent: :destroy,
    autosave: true,
    foreign_key: :channel_association_id
  has_one :category,
    dependent: :destroy,
    autosave: true

  def assign_from_item(item)
    self.item_id = item.id
  end

  def assign_from_account(account)
    self.channel_id = account.channel_id
  end
end
