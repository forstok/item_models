# frozen_string_literal: true

class AvailabilityOption < ItemApplicationRecord
  self.table_name = 'item_availability_options'
end
