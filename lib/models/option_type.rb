# frozen_string_literal: true

class OptionType < ItemApplicationRecord
  self.table_name = 'item_variant_option_types'

  belongs_to :item
  has_many :options, class_name: 'VariantOption', foreign_key: :option_type_id
end
