# frozen_string_literal: true

require 'concerns/restorable'
class Item < ItemApplicationRecord
  include ::Restorable

  has_many :variants
  has_many :item_listings
  has_many :bundles
  has_many :images
  has_many :option_types, foreign_key: :item_id

  def assign_from_profile(profile)
    self.profile_id = profile.id
  end
end
