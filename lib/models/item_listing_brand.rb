# frozen_string_literal: true

require 'concerns/restorable'
class ItemListingBrand < ItemApplicationRecord
  self.table_name = 'item_channel_association_brands'
  include ::Restorable

  belongs_to :item_listing, foreign_key: :channel_association_id
  belongs_to :shopee,
    class_name: 'ShopeeBrand',
    foreign_key: :shopee_brand_id,
    optional: true
  belongs_to :tokopedia,
    class_name: 'ShopeeBrand',
    foreign_key: :shopee_brand_id,
    optional: true
end
