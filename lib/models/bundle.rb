# frozen_string_literal: true

require 'concerns/restorable'
class Bundle < ItemApplicationRecord
  self.table_name = 'item_bundles'
  include ::Restorable

  belongs_to :variant
  belongs_to :item
  has_many :bundle_variants
end
