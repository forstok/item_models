# frozen_string_literal: true

class ShopeeShippingProvider < ItemApplicationRecord
  self.table_name = 'channel_shopee_shipping_providers'
end
