# frozen_string_literal: true

class ShopeeCategory < ItemApplicationRecord
  self.table_name = 'channel_shopee_categories'

  belongs_to :parent_category,
    class_name: 'ShopeeCategory',
    foreign_key: :parent_category_id,
    optional: true
  belongs_to :primary_category,
    class_name: 'ShopeeCategory',
    foreign_key: :primary_category_id,
    optional: true
  has_many :child_categories,
    class_name: 'ShopeeCategory',
    foreign_key: :parent_category_id,
    dependent: :destroy

  has_many :item_channel_association_categories,
    class_name: '::ItemListingCategory',
    foreign_key: :shopee_category_id
end
