module Restorable
  extend ActiveSupport::Concern

  included do
    acts_as_paranoid column: :removed, sentinel_value: false
    acts_as_paranoid column: :removed_at, sentinel_value: false

    # It seems paranoia bug: method_missing `deleted_at`
    # when restore with `recovery_window`
    alias_attribute :deleted_at, :removed_at

    private

    def paranoia_restore_attributes
      {
        removed_at: nil,
        removed: false
      }
    end

    def paranoia_destroy_attributes
      {
        removed_at: current_time_from_proper_timezone,
        removed: nil
      }
    end
  end
end
