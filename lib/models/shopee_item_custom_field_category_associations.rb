# frozen_string_literal: true

class ShopeeItemCustomFieldCategoryAssociation < ItemApplicationRecord
  self.table_name = 'channel_shopee_item_custom_field_category_associations'
end