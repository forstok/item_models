# frozen_string_literal: true

class VariantListingPriceHistory < ItemApplicationRecord
  self.table_name = 'item_channel_association_variant_association_stock_allocations'

  belongs_to :variant_listing, foreign_key: :variant_association_id
end
