# frozen_string_literal: true

require 'concerns/restorable'
class Variant < ItemApplicationRecord
  self.table_name = 'item_variants'
  include ::Restorable

  belongs_to :item
  has_many :variant_images, foreign_key: :item_variant_id
  has_many :bundles
  has_many :variant_listings, foreign_key: :variant_id
  has_many :item_listing_variant_images, foreign_key: :variant_id
  has_many :item_listing_variant_custom_field, foreign_key: :variant_id
  has_many :item_listing_custom_field, foreign_key: :variant_id
  has_one :master_catalog, foreign_key: :variant_id
  has_many :option_associations, class_name: 'VariantOptionAssociation', foreign_key: :variant_id
  has_many :options, through: :option_associations, source: :variant_option

  def assign_from_item(item)
    self.item_id = item.id
    self.profile_id = item.profile_id
  end
end
