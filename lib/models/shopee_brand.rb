# frozen_string_literal: true

class ShopeeBrand < ItemApplicationRecord
  self.table_name = 'channel_shopee_brands'

  has_many :item_listing_brands,
    class_name: 'ItemListingBrand',
    foreign_key: :shopee_brand_id
end
