# frozen_string_literal: true

class MasterCatalog < ItemApplicationRecord
  self.table_name = 'item_variant_master_assocs'

  belongs_to :variant
  has_many :master_catalog_images, foreign_key: :master_assoc_id
end
