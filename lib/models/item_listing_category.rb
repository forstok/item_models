# frozen_string_literal: true

require 'concerns/restorable'
class ItemListingCategory < ItemApplicationRecord
  self.table_name = 'item_channel_association_categories'
  include ::Restorable

  belongs_to :item_listing, foreign_key: :channel_association_id
  belongs_to :shopee,
    class_name: 'ShopeeCategory',
    foreign_key: :shopee_category_id,
    optional: true
  belongs_to :tokopedia,
    class_name: 'TokopediaCategory',
    foreign_key: :tokopedia_category_id,
    optional: true
end
