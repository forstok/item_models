# frozen_string_literal: true

require 'concerns/restorable'
class VariantImage < ItemApplicationRecord
  self.table_name = 'item_variant_images'
  include ::Restorable

  belongs_to :image
  belongs_to :variant
end
