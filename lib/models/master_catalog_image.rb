# frozen_string_literal: true

class MasterCatalogImage < ItemApplicationRecord
  self.table_name = 'item_variant_master_image_assocs'

  belongs_to :master_catalog, foreign_key: :master_assoc_id
  belongs_to :image, foreign_key: :image_id
end
