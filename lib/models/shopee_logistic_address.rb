# frozen_string_literal: true

class ShopeeLogisticAddress < ItemApplicationRecord
  self.table_name = 'channel_shopee_logistic_addresses'
end
