# frozen_string_literal: true

class ShopeeItemCustomFieldOption < ItemApplicationRecord
  self.table_name = 'channel_shopee_item_custom_field_options'

  belongs_to :custom_field,
    class_name: 'ShopeeItemCustomField',
    foreign_key: :custom_field_id,
    optional: true
end
