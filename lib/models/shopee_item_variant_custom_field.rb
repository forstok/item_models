# frozen_string_literal: true

class ShopeeItemVariantCustomField < ItemApplicationRecord
  self.table_name = 'channel_shopee_variant_custom_fields'
end
