# frozen_string_literal: true

class ShopeeItemCustomField < ItemApplicationRecord
  self.table_name = 'channel_shopee_item_custom_fields'

  has_many :associated_custom_fields, class_name: 'ItemListingCustomField'
  has_many :options, foreign_key: :custom_field_id, class_name: 'ShopeeItemCustomFieldOption'
end
