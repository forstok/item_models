# frozen_string_literal: true

require 'concerns/restorable'
class ItemListingCustomField < ItemApplicationRecord
  self.table_name = 'item_channel_association_custom_fields'
  include ::Restorable

  belongs_to :item_listing, foreign_key: :channel_association_id
  belongs_to :variant
  belongs_to :shopee,
    class_name: 'ShopeeItemCustomField',
    foreign_key: :shopee_item_custom_field_id,
    optional: true

  serialize :value
end
