# frozen_string_literal: true

class ItemListingVariantImage < ItemApplicationRecord
  self.table_name = 'item_channel_association_variant_images'

  belongs_to :image,
    class_name: 'Image',
    optional: true
end
