# frozen_string_literal: true

require 'concerns/restorable'
class Image < ItemApplicationRecord
  self.table_name = 'item_images'
  include ::Restorable

  belongs_to :item
  has_many :item_listing_images
  has_many :variant_images, foreign_key: :item_image_id
  has_many :master_catalog_images
  has_many :item_listing_variant_custom_fields

  has_attached_file :image,
    path: '/system/item/:class/:attachment/:id_partition/:style/:fingerprint.:extension',
    default_url: lambda { |attach|
      attach.instance.url || '/public/missing/missing-item.jpg'
    },
    styles: lambda { |attachment|
      object        = attachment.instance.image.queued_for_write[:original]
      width, height =
        if object
          FastImage.size(object) || [0,0]
        else
          [(attachment.instance.width || 500), (attachment.instance.height || 500)]
        end

      dim           = [width, height]
      clamped_dim   = dim.max && [600, dim.max, 2000].sort[1]
      big_clamped_dim = dim.max && [1100, dim.max, 2000].sort[1]

      # max hxw and ratio (1100 x 762)
      ratio_381_550 = 1100/762.to_f
      real_ratio = height/width.to_f

      # If real ratio is smaller, then width is thicker than desired width, therefor
      # adjust height to fit desired ratio.
      if real_ratio < ratio_381_550
        w_381_550 = width
        h_381_550 = width * ratio_381_550
      else
        w_381_550 = height / ratio_381_550
        h_381_550 = height
      end

      {
        xlarge: {
          geometry: '1600x1600>',
          format: :jpg,
          convert_options: '-strip -interlace Plane -background white -alpha remove -flatten -alpha off'
        },
        large: {
          geometry: '1000x1000>',
          format: :jpg,
          convert_options: '-strip -interlace Plane -background white -alpha remove -flatten -alpha off'
        },
        medium: {
          geometry: '800x800>',
          format: :jpg,
          convert_options: '-strip -interlace Plane -background white -alpha remove -flatten -alpha off'
        },
        small: {
          geometry: '500x500>',
          format: :jpg,
          convert_options: '-strip -interlace Plane -background white -alpha remove -flatten -alpha off'
        },
        thumb: {
          geometry: '100x100>',
          format: :jpg,
          convert_options: '-strip -interlace Plane -background white -alpha remove -flatten -alpha off'
        },
        marketplace: {
          geometry: '500x500<',
          format: :jpg,
          convert_options: '-strip -interlace Plane -background white -alpha remove -flatten -alpha off'
        },
        # Lazada needs colorspace to be sRGB. CMYK colorspace will produce error
        square: {
          geometry: dim.max && dim.max < 600 ? '600x600<' : '2000x2000>',
          format: :jpg,
          convert_options: "-strip -colorspace sRGB -interlace Plane -background white -alpha remove -flatten -alpha off -gravity center -extent #{clamped_dim}x#{clamped_dim}"
        },
        # Zalora
        big_square: {
          geometry: dim.max && dim.max < 1100 ? '1100x1100<' : '2000x2000>',
          format: :jpg,
          convert_options: "-strip -colorspace sRGB -interlace Plane -background white -alpha remove -flatten -alpha off -gravity center -extent #{big_clamped_dim}x#{big_clamped_dim}"
        },
        # lyke
        ratio_381_550: {
          geometry: w_381_550 <= 762 ? '762x1100<' : '762x1100>',
          format: :jpeg,
          convert_options: "-strip -colorspace sRGB -interlace Plane -background white -alpha remove -flatten -alpha off -gravity center -extent 762x1100"
        },
        # elevenia
        width_780_or_less: {
          geometry: '780x>',
          format: :jpg,
          convert_options: '-strip -interlace Plane -background white -alpha remove -flatten -alpha off'
        }
      }
    }
end
