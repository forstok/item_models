# frozen_string_literal: true

require 'concerns/restorable'
class ItemListingImage < ItemApplicationRecord
  self.table_name = 'item_channel_association_images'
  include ::Restorable

  belongs_to :image
  belongs_to :item_listing, foreign_key: :channel_association_id
end
