# frozen_string_literal: true

class Activity < ItemApplicationRecord
  self.table_name = 'item_activities'
end
