# frozen_string_literal: true

require 'concerns/restorable'
class VariantListing < ItemApplicationRecord
  self.table_name = 'item_channel_association_variant_associations'
  include ::Restorable

  belongs_to :variant
  belongs_to :item_listing, foreign_key: :channel_association_id
  has_one :variant_listing_stock_allocation,
    class_name: 'VariantListingStockAllocation',
    foreign_key: :variant_association_id
  has_many :variant_listing_price_histories, foreign_key: :variant_association_id
  belongs_to :tokopedia_showcase, optional: true

  serialize :option2_value
  serialize :option_value
  serialize :name_option
  serialize :qc_reason
  serialize :last_log
  serialize :bukalapak_free_shipping
  serialize :shopee_shipping_providers
  serialize :description
  serialize :description_unique_selling_point

  def item_listing_custom_fields
    ItemListingCustomField.where("channel_association_id = ? AND variant_id = ?", self.channel_association_id, self.variant_id)
  end

  def item_listing_variant_custom_fields
    ItemListingVariantCustomField.where("channel_association_id = ? AND variant_id = ?", self.channel_association_id, self.variant_id)
  end

  def item_listing_variant_images
    ItemListingVariantImage.where(
      "channel_association_id = ? AND variant_id = ? AND account_id = ?", self.channel_association_id, self.variant_id, self.profile_channel_association_id
    )
  end

  def assign_from_variant(variant)
    self.variant_id = variant.id
  end

  def assign_from_item_listing(item)
    self.channel_association_id = item.id
  end

  def assign_from_account(account)
    self.profile_channel_association_id = account.id
    self.channel_id = account.channel_id
  end
end
