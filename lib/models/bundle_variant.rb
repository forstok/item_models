# frozen_string_literal: true

require 'concerns/restorable'
class BundleVariant < ItemApplicationRecord
  self.table_name = 'item_bundle_variants'
  include ::Restorable

  belongs_to :bundle
  belongs_to :variant
end
