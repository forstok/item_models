# frozen_string_literal: true

class ItemApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  connects_to database: { writing: :item_model, reading: :item_model }
end
