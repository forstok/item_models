# frozen_string_literal: true

class TokopediaItemVariantCustomField < ItemApplicationRecord
  self.table_name = 'channel_tokopedia_item_variant_custom_fields'
end
